# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Evento'
        db.create_table(u'eventos_evento', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('evento', self.gf('django.db.models.fields.CharField')(max_length=400)),
            ('objetivo', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('lugar', self.gf('django.db.models.fields.CharField')(max_length=400, null=True, blank=True)),
            ('contacto', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=35, null=True, blank=True)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=35, null=True, blank=True)),
            ('apartado', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('correo', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('web', self.gf('django.db.models.fields.CharField')(max_length=160, null=True, blank=True)),
            ('fechaingreso', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('archivo', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('logotipo', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('organiza', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('uri', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('ciudad', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'eventos', ['Evento'])

        # Adding M2M table for field idtematica on 'Evento'
        m2m_table_name = db.shorten_name(u'eventos_evento_idtematica')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('evento', models.ForeignKey(orm[u'eventos.evento'], null=False)),
            ('tematica', models.ForeignKey(orm[u'publicaciones.tematica'], null=False))
        ))
        db.create_unique(m2m_table_name, ['evento_id', 'tematica_id'])


    def backwards(self, orm):
        # Deleting model 'Evento'
        db.delete_table(u'eventos_evento')

        # Removing M2M table for field idtematica on 'Evento'
        db.delete_table(db.shorten_name(u'eventos_evento_idtematica'))


    models = {
        u'eventos.evento': {
            'Meta': {'object_name': 'Evento'},
            'apartado': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'archivo': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'contacto': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'correo': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'evento': ('django.db.models.fields.CharField', [], {'max_length': '400'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '35', 'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'fechaingreso': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idtematica': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['publicaciones.Tematica']", 'null': 'True', 'blank': 'True'}),
            'logotipo': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'lugar': ('django.db.models.fields.CharField', [], {'max_length': '400', 'null': 'True', 'blank': 'True'}),
            'objetivo': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'organiza': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '35', 'null': 'True', 'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '160', 'null': 'True', 'blank': 'True'})
        },
        u'publicaciones.tematica': {
            'Meta': {'object_name': 'Tematica'},
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'orden': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tematica': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        }
    }

    complete_apps = ['eventos']