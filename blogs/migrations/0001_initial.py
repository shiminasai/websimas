# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Catblog'
        db.create_table(u'blogs_catblog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('catblog', self.gf('django.db.models.fields.CharField')(max_length=350, null=True, blank=True)),
        ))
        db.send_create_signal(u'blogs', ['Catblog'])

        # Adding model 'Blog'
        db.create_table(u'blogs_blog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('blog', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('idautor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['otras.Autor'], null=True, blank=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('foto', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('uri', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('idcatblog', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['blogs.Catblog'], null=True, blank=True)),
            ('resumen', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('tags', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
        ))
        db.send_create_signal(u'blogs', ['Blog'])

        # Adding M2M table for field tematica on 'Blog'
        m2m_table_name = db.shorten_name(u'blogs_blog_tematica')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('blog', models.ForeignKey(orm[u'blogs.blog'], null=False)),
            ('tematica', models.ForeignKey(orm[u'publicaciones.tematica'], null=False))
        ))
        db.create_unique(m2m_table_name, ['blog_id', 'tematica_id'])


    def backwards(self, orm):
        # Deleting model 'Catblog'
        db.delete_table(u'blogs_catblog')

        # Deleting model 'Blog'
        db.delete_table(u'blogs_blog')

        # Removing M2M table for field tematica on 'Blog'
        db.delete_table(db.shorten_name(u'blogs_blog_tematica'))


    models = {
        u'blogs.blog': {
            'Meta': {'object_name': 'Blog'},
            'blog': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idautor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['otras.Autor']", 'null': 'True', 'blank': 'True'}),
            'idcatblog': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['blogs.Catblog']", 'null': 'True', 'blank': 'True'}),
            'resumen': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'tags': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'tematica': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['publicaciones.Tematica']", 'null': 'True', 'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'blogs.catblog': {
            'Meta': {'object_name': 'Catblog'},
            'catblog': ('django.db.models.fields.CharField', [], {'max_length': '350', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'otras.autor': {
            'Meta': {'object_name': 'Autor'},
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'publicaciones.tematica': {
            'Meta': {'object_name': 'Tematica'},
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'orden': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tematica': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        }
    }

    complete_apps = ['blogs']