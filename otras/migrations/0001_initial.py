# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Aliado'
        db.create_table(u'otras_aliado', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('correo', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('descripcion', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('logo', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('aliado', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('uri', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('web', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
        ))
        db.send_create_signal(u'otras', ['Aliado'])

        # Adding model 'Servicio'
        db.create_table(u'otras_servicio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('servicio', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('descripcion', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('imagen', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('uri', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
        ))
        db.send_create_signal(u'otras', ['Servicio'])

        # Adding model 'Sistema'
        db.create_table(u'otras_sistema', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sistema', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('foto', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'otras', ['Sistema'])

        # Adding model 'Financiadores'
        db.create_table(u'otras_financiadores', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('logo', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True)),
            ('web', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('orden', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'otras', ['Financiadores'])

        # Adding model 'Autor'
        db.create_table(u'otras_autor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('autor', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('imagen', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
        ))
        db.send_create_signal(u'otras', ['Autor'])

        # Adding model 'Pais'
        db.create_table(u'otras_pais', (
            ('idpais', self.gf('django.db.models.fields.CharField')(max_length=2, primary_key=True)),
            ('pais', self.gf('django.db.models.fields.CharField')(max_length=80, null=True, blank=True)),
            ('region', self.gf('django.db.models.fields.CharField')(max_length=2, null=True, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=3, null=True, blank=True)),
        ))
        db.send_create_signal(u'otras', ['Pais'])

        # Adding model 'UbicacionTrabajor'
        db.create_table(u'otras_ubicaciontrabajor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ubicacion', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'otras', ['UbicacionTrabajor'])

        # Adding model 'Trabajadores'
        db.create_table(u'otras_trabajadores', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombres', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('foto', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('correo', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('cargo', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('ubicacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['otras.UbicacionTrabajor'])),
        ))
        db.send_create_signal(u'otras', ['Trabajadores'])


    def backwards(self, orm):
        # Deleting model 'Aliado'
        db.delete_table(u'otras_aliado')

        # Deleting model 'Servicio'
        db.delete_table(u'otras_servicio')

        # Deleting model 'Sistema'
        db.delete_table(u'otras_sistema')

        # Deleting model 'Financiadores'
        db.delete_table(u'otras_financiadores')

        # Deleting model 'Autor'
        db.delete_table(u'otras_autor')

        # Deleting model 'Pais'
        db.delete_table(u'otras_pais')

        # Deleting model 'UbicacionTrabajor'
        db.delete_table(u'otras_ubicaciontrabajor')

        # Deleting model 'Trabajadores'
        db.delete_table(u'otras_trabajadores')


    models = {
        u'otras.aliado': {
            'Meta': {'object_name': 'Aliado'},
            'aliado': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'correo': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'descripcion': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'})
        },
        u'otras.autor': {
            'Meta': {'object_name': 'Autor'},
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'otras.financiadores': {
            'Meta': {'object_name': 'Financiadores'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'orden': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'otras.pais': {
            'Meta': {'object_name': 'Pais'},
            'idpais': ('django.db.models.fields.CharField', [], {'max_length': '2', 'primary_key': 'True'}),
            'pais': ('django.db.models.fields.CharField', [], {'max_length': '80', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True', 'blank': 'True'})
        },
        u'otras.servicio': {
            'Meta': {'object_name': 'Servicio'},
            'descripcion': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'servicio': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'otras.sistema': {
            'Meta': {'object_name': 'Sistema'},
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sistema': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'otras.trabajadores': {
            'Meta': {'object_name': 'Trabajadores'},
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'foto': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombres': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'ubicacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['otras.UbicacionTrabajor']"})
        },
        u'otras.ubicaciontrabajor': {
            'Meta': {'object_name': 'UbicacionTrabajor'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ubicacion': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['otras']