# -*- coding: utf-8 -*-

from django.db import models
from websimas.utils import get_file_path
from sorl.thumbnail import ImageField
from ckeditor.fields import RichTextField
from django.template.defaultfilters import slugify

# Create your models here.

class Aliado(models.Model):
    correo = models.CharField(max_length=255, blank=True, null=True)
    direccion = models.CharField(max_length=255, blank=True, null=True)
    descripcion = RichTextField(blank=True, null=True)
    logo = ImageField('Logo oficial', upload_to=get_file_path, blank=True, 
        null=True)
    aliado = models.CharField(max_length=255, blank=True, null=True)
    uri = models.CharField(max_length=500, blank=True, null=True, editable=False)
    web = models.CharField(max_length=300, blank=True, null=True)

    fileDir = 'logosAliados/'

    def save(self, *args, **kwargs):
        self.uri = (slugify(self.aliado))
        super(Aliado, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.aliado

    class Meta:
        verbose_name=u'Aliado'
        verbose_name_plural=u'Aliados'

class Servicio(models.Model):
    servicio = models.CharField(max_length=255, blank=True, null=True)
    descripcion = RichTextField(blank=True, null=True)
    imagen = ImageField('Foto', upload_to=get_file_path, blank=True, null=True)
    uri = models.CharField(max_length=500, blank=True, null=True)

    fileDir = 'fotoServicio/'

    def __unicode__(self):
        return self.servicio

    class Meta:
        verbose_name=u'Servicio'
        verbose_name_plural=u'Servicios'

class Sistema(models.Model):
    sistema = models.CharField(max_length=255, blank=True, null=True)
    foto = ImageField('Foto sistema', upload_to=get_file_path, blank=True, null=True)
    url = models.CharField(max_length=255, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)

    fileDir = 'fotoSistema/'

    def __unicode__(self):
        return self.sistema

    class Meta:
        verbose_name=u'Sistema'
        verbose_name_plural=u'Sistemas'

class Financiadores(models.Model):
    logo = models.CharField(max_length=500, blank=True, null=True)
    nombre = models.CharField(max_length=120, blank=True, null=True)
    web = models.CharField(max_length=500, blank=True, null=True)
    orden = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name=u'Financiador'
        verbose_name_plural=u'Financiadores'

class Autor(models.Model):
    autor = models.CharField(max_length=500, blank=True, null=True)
    email = models.CharField(max_length=150, blank=True, null=True)
    imagen = models.CharField(max_length=500, blank=True, null=True)

    def __unicode__(self):
        return self.autor

    class Meta:
        verbose_name=u'Autor'
        verbose_name_plural=u'Autores'


class Pais(models.Model):
    idpais = models.CharField(primary_key=True, max_length=2)
    pais = models.CharField(max_length=80, blank=True, null=True)
    region = models.CharField(max_length=2, blank=True, null=True)
    telefono = models.CharField(max_length=3, blank=True, null=True)
    
    def __unicode__(self):
        return self.pais

    class Meta:
        verbose_name=u'Pais'
        verbose_name_plural=u'Paises'

class UbicacionTrabajor(models.Model):
    ubicacion = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = u'Ubicación del trabajador'
        ordering = ('-ubicacion',)

    def __unicode__(self):
        return self.ubicacion

class Trabajadores(models.Model):
    nombres = models.CharField('Nombres y apellidos', max_length=250)
    foto = ImageField(upload_to=get_file_path, blank=True, null=True)
    correo = models.EmailField()
    cargo = models.CharField('cargo', max_length=50)
    ubicacion = models.ForeignKey(UbicacionTrabajor)

    fileDir = 'fotoTrabajador/'

    class Meta:
        verbose_name_plural = u'Trabajadores'

    def __unicode__(self):
        return self.nombres

CHOICE_MENU = (
                (1, "quienes-somos"),
                (2, "publicaciones"),
                (3, "portafolio"),
                (4, "noticias"),
                (5, "blogs"),
                (6, "contactenos"),
                (7, "historia"),
    )

class MenuFoto(models.Model):
    menu = models.IntegerField(choices=CHOICE_MENU)
    titulo = models.CharField(max_length=250)
    foto = ImageField(upload_to=get_file_path)

    fileDir = 'fotoHeader/'

    def __unicode__(self):
        return self.titulo
