from django.db import models
from publicaciones.models import Tematica
from ckeditor.fields import RichTextField
from websimas.utils import get_file_path
from sorl.thumbnail import ImageField
from django.template.defaultfilters import slugify

# Create your models here.
TIPO_CHOICES = (
                    (1,'Texto'),
                    (2,'Video'),
                )

class Noticia(models.Model):
    noticia = models.CharField(max_length=350, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    importante = models.NullBooleanField('En portada')
    fuente = models.CharField(max_length=100, blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True)
    texto = RichTextField(blank=True, null=True)
    claves = models.CharField(max_length=255, blank=True, null=True)
    descripcion = models.TextField(blank=True, null=True)
    idautor = models.ForeignKey('otras.Autor',verbose_name=u'Publicado por: ', 
                               blank=True, null=True)
    uri = models.CharField(max_length=500, blank=True, null=True)
    foto = ImageField('Foto principal', upload_to=get_file_path, blank=True, null=True)
    url = models.URLField(blank=True, null=True)
    tipo = models.IntegerField(choices=TIPO_CHOICES,blank=True, null=True)
    idtematica = models.ManyToManyField(Tematica, verbose_name=u'Tematicas', blank=True, null=True)
    credito = models.CharField(max_length=200, blank=True, null=True)
    #autor = models.CharField(max_length=200, blank=True, null=True)
    resumen = models.TextField(blank=True, null=True)
    #idtema = models.IntegerField(blank=True, null=True)
    titarchivo1 = models.CharField('Titulo 1', max_length=200, blank=True, null=True)
    titarchivo2 = models.CharField('Titulo 2', max_length=200, blank=True, null=True)
    archivo = models.CharField('Titulo 3', max_length=500, blank=True, null=True)
    archivo1 = models.FileField(upload_to=get_file_path, blank=True, null=True)
    archivo2 = models.FileField(upload_to=get_file_path, blank=True, null=True)
    archivo3 = models.FileField(upload_to=get_file_path, blank=True, null=True)

    fileDir = 'noticias/'

    def save(self, *args, **kwargs):
        self.uri = (slugify(self.noticia))
        super(Noticia, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = u"Noticias"

    def __unicode__(self):
        return self.noticia