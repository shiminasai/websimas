from django.shortcuts import render, get_object_or_404
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView
import json
from django.core.mail import send_mail
from django.http import HttpResponse
#from django.views.generic.detail import DetailView
from .models import Noticia
from .forms import ContactForm
from publicaciones.models import Publicacion
from blogs.models import Blog
from portafolio.models import Portafolio
from portada.models import FotoPortada
from historia.models import Historias
from otras.models import *
from contacto.models import *
#from bs4 import BeautifulSoup, Tag
from endless_pagination.decorators import page_template
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


#class PortafolioPageView(TemplateView):
#    template_name = "portafolio.html"

#    def get_context_data(self, **kwargs):
#        context = super(PortafolioPageView, self).get_context_data(**kwargs)
#        context['portafolios'] = Portafolio.objects.order_by('-fecha')

#        return context

class ListPortfolioView(ListView):
    model = Portafolio
    queryset =  Portafolio.objects.order_by('-fecha')
    paginate_by = 4

    def get_context_data(self, **kwargs):
        context = super(ListPortfolioView, self).get_context_data(**kwargs)
        context['foto_portafolio'] = MenuFoto.objects.get(menu=3)

        return context

# @page_template('portafolio_page.html')
# def portafolio_page_view(request, template='portafolio.html', extra_context=None):
#     context = {
#         'portafolios': Portafolio.objects.order_by('-id'),
#         'foto_portafolio': MenuFoto.objects.get(menu=3)
#     }
#     if extra_context is not None:
#         context.update(extra_context)

#     return render(request, template, context)


class ListServiceView(ListView):
    model = Servicio
    queryset = Servicio.objects.all()
    paginate_by = 4

    def get_context_data(self, **kwargs):
        context = super(ListServiceView, self).get_context_data(**kwargs)
        context['foto_portafolio'] = MenuFoto.objects.get(menu=3)

        return context

# def portafolio_serv_view(request, template='portafolio_serv.html', extra_context=None):
#     servicios = Servicio.objects.all()
#     context = {
#         'portafolios': Portafolio.objects.order_by('-id'),
#         'foto_portafolio': MenuFoto.objects.get(menu=3),
#         'servicios' : servicios,
#     }
#     if extra_context is not None:
#         context.update(extra_context)

#     return render(request, template, context)


class HistoriaPageView(TemplateView):
    template_name = "historia.html"

    def get_context_data(self, **kwargs):
        context = super(HistoriaPageView, self).get_context_data(**kwargs)
        context['historias'] = Historias.objects.all()

        return context


class QuienesPageView(TemplateView):
    template_name = "quienesomos.html"

    def get_context_data(self, **kwargs):
        context = super(QuienesPageView, self).get_context_data(**kwargs)
        context['foto_quienes'] = MenuFoto.objects.get(menu=1)

        return context

class ContactenosPageView(TemplateView):
    template_name = "contacto.html"

    def get_context_data(self, **kwargs):
        context = super(ContactenosPageView, self).get_context_data(**kwargs)
        context['form_contact'] = ContactForm()
        context['ubicacion_trabajador'] = UbicacionTrabajor.objects.all()
        context['personal'] = Trabajadores.objects.all()
        context['foto_contacto'] = MenuFoto.objects.get(menu=6)
        context['contacto'] = Contacto.objects.get()

        return context

    
class HomePageView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['latest_news'] = Noticia.objects.order_by('-fecha')[:3]
        context['latest_pub'] = Publicacion.objects.filter(enportada=True).order_by('-id')[:4]
        context['latest_blogs'] = Blog.objects.order_by('-fecha')[:3]
        context['latest_portafolio'] = Portafolio.objects.order_by('-fecha')[:3]
        context['latest_portada'] = FotoPortada.objects.all()
        context['latest_service'] = Servicio.objects.order_by('-id')[:3]
        context['latest_aliados'] = Aliado.objects.order_by('-id')

        return context


class ListNewsView(ListView):
    model = Noticia
    queryset = Noticia.objects.order_by('-fecha')
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(ListNewsView, self).get_context_data(**kwargs)
        context['foto_noticia'] = MenuFoto.objects.get(menu=4)

        return context


def DetailNewsView(request, template='noticias/detalle_noticia.html', id=None, uri=None):
    nota = get_object_or_404(Noticia,id=id)

    lista_tematicas = nota.idtematica.all()[0]

    try:
        noticias_relacionadas = Noticia.objects.filter(idtematica=lista_tematicas).exclude(id=nota.id).order_by('-id')[:3]
        nota_all = Noticia.objects.filter(idtematica=lista_tematicas).filter(tipo=2).exclude(id=nota.id).order_by('-fecha')[:3]

    except:
        pass

    foto_noticia = MenuFoto.objects.get(menu=4)
    
    return render(request, template, locals())


def contacto_ajax(request):
    if request.is_ajax():
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['asunto']
            message = form.cleaned_data['mensaje']
            sender = form.cleaned_data['correo']

            recipients = ['crocha09.09@gmail.com','manejo-informacion@simas.org.ni']

            send_mail(subject, message, sender, recipients)
            return HttpResponse( json.dumps( 'exito' ), mimetype='application/json' )
        else:
            return HttpResponse( json.dumps( 'falso' ), mimetype='application/json' )

# from haystack.query import SearchQuerySet
# def search_publicaciones(request, template="search/search.html"):
#     query = request.POST.get('s', '')
#     object_list1 = SearchQuerySet()
    
#     paginator = Paginator(object_list1, 20) # Show 25 contacts per page

#     page = request.GET.get('page')
#     try:
#         object_list = paginator.page(page)
#     except PageNotAnInteger:
#         # If page is not an integer, deliver first page.
#         object_list = paginator.page(1)
#     except EmptyPage:
#         # If page is out of range (e.g. 9999), deliver last page of results.
#         object_list = paginator.page(paginator.num_pages)
#     return render(request,template,{'object_list': object_list, 'query': query})


#def sopa(texto):

#    soup = BeautifulSoup(texto)

#    for tag in soup.find_all('<p>&nbsp;</p>','<p>','<span>'):
#        tag.replaceWith('')

#    return soup.get_text()

#def funcion_limpiar(request):

#    texto = Noticia.objects.all()
#    for text in texto:
#         a = sopa(text.texto)
#         text.texto = a
#         text.save()

#    return 1