from django.conf.urls import patterns, url

from .views import HomePageView, ListNewsView, DetailNewsView, HistoriaPageView, QuienesPageView, ContactenosPageView, ListServiceView, ListPortfolioView

urlpatterns = patterns('noticias.views',
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^portafolio/servicios$',ListServiceView.as_view(), name='servicios'),
    url(r'^portafolio/sistemas$',ListPortfolioView.as_view(), name='portafolio1'),
    url(r'^historia-simas/$', HistoriaPageView.as_view(), name='historia'),
    url(r'^quienes-somos/$', QuienesPageView.as_view(), name='somos'),
    url(r'^noticias/$', ListNewsView.as_view(), name='noticias'),
    url(r'^noticias/(?P<id>[0-9]+)/(?P<uri>[-_\w]+)/$', 'DetailNewsView', name='detalle-noticia'),
    url(r'^contactenos/$', ContactenosPageView.as_view(), name='contactenos'),
    #url(r'^buscar-publicacion/$', 'search_publicaciones', name='search_publicaciones'),


    #url(r'^prueba/$', 'funcion_limpiar', name='limpiar'),
)
