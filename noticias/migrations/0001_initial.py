# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Noticia'
        db.create_table(u'noticias_noticia', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('noticia', self.gf('django.db.models.fields.CharField')(max_length=350, null=True, blank=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('importante', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('fuente', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('texto', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('claves', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('idautor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['otras.Autor'], null=True, blank=True)),
            ('uri', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('foto', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('credito', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('resumen', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('titarchivo1', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('titarchivo2', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('archivo', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('archivo1', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('archivo2', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('archivo3', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'noticias', ['Noticia'])

        # Adding M2M table for field idtematica on 'Noticia'
        m2m_table_name = db.shorten_name(u'noticias_noticia_idtematica')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('noticia', models.ForeignKey(orm[u'noticias.noticia'], null=False)),
            ('tematica', models.ForeignKey(orm[u'publicaciones.tematica'], null=False))
        ))
        db.create_unique(m2m_table_name, ['noticia_id', 'tematica_id'])


    def backwards(self, orm):
        # Deleting model 'Noticia'
        db.delete_table(u'noticias_noticia')

        # Removing M2M table for field idtematica on 'Noticia'
        db.delete_table(db.shorten_name(u'noticias_noticia_idtematica'))


    models = {
        u'noticias.noticia': {
            'Meta': {'object_name': 'Noticia'},
            'archivo': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'archivo1': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'archivo2': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'archivo3': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'claves': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'credito': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'fuente': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idautor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['otras.Autor']", 'null': 'True', 'blank': 'True'}),
            'idtematica': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['publicaciones.Tematica']", 'null': 'True', 'blank': 'True'}),
            'importante': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'noticia': ('django.db.models.fields.CharField', [], {'max_length': '350', 'null': 'True', 'blank': 'True'}),
            'resumen': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'texto': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'titarchivo1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'titarchivo2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'otras.autor': {
            'Meta': {'object_name': 'Autor'},
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'publicaciones.tematica': {
            'Meta': {'object_name': 'Tematica'},
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'orden': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tematica': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        }
    }

    complete_apps = ['noticias']