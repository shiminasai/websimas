from django.views.generic import TemplateView
from django.conf import settings

class SearchView(TemplateView):
    template_name = 'django_google_cse/default.html'