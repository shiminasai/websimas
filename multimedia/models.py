from django.db import models

# Create your models here.

class Cataudio(models.Model):
    #idcataudio = models.IntegerField(primary_key=True)
    cataudio = models.CharField(max_length=250, blank=True)
    ordencataudio = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return self.cataudio

class Audio(models.Model):
    #idaudio = models.IntegerField(primary_key=True)
    audio = models.CharField(max_length=255, blank=True)
    embed = models.TextField(blank=True)
    archivomp3 = models.CharField(max_length=500, blank=True)
    fecha_ini = models.DateField(blank=True, null=True)
    idcataudio = models.ForeignKey('Cataudio', blank=True, null=True)

    def __unicode__(self):
        return self.audio


class Estadovideo(models.Model):
    #idestadovideo = models.IntegerField(primary_key=True)
    estadovideo = models.CharField(max_length=30, blank=True)

    def __unicode__(self):
        return self.estadovideo

class Video(models.Model):
    #idvideo = models.IntegerField(primary_key=True)
    video = models.CharField(max_length=255, blank=True, null=True)
    texto = models.TextField(blank=True, null=True)
    url = models.CharField(max_length=300, blank=True, null=True)
    claves = models.CharField(max_length=255, blank=True, null=True)
    creacion = models.DateTimeField(blank=True, null=True)
    archivo = models.CharField(max_length=500, blank=True, null=True)
    idusuario = models.IntegerField(blank=True, null=True)
    preview = models.CharField(max_length=500, blank=True, null=True)
    youtube = models.CharField(max_length=500, blank=True, null=True)
    idtematica = models.IntegerField(blank=True, null=True)
    cidoc = models.NullBooleanField()
    codigo = models.CharField(max_length=50, blank=True, null=True)
    duracion = models.CharField(max_length=50, blank=True, null=True)
    anho = models.CharField(max_length=4, blank=True, null=True)
    realizacion = models.CharField(max_length=255, blank=True, null=True)
    idestadovideo = models.IntegerField(blank=True, null=True)
    pais = models.CharField(max_length=50, blank=True, null=True)
    googleid = models.CharField(max_length=500, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    titulo2 = models.CharField(max_length=300, blank=True, null=True)

    def __unicode__(self):
        return self.video