from django.contrib import admin
from .models import *

class VideoAdmin(admin.ModelAdmin):
	list_display = ('id','video','codigo','texto','realizacion')
	search_fields = ('video','codigo','texto')

# Register your models here.
admin.site.register(Cataudio)
admin.site.register(Audio)
admin.site.register(Estadovideo)
admin.site.register(Video, VideoAdmin)
