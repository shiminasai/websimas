# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Cataudio'
        db.create_table(u'multimedia_cataudio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cataudio', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('ordencataudio', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'multimedia', ['Cataudio'])

        # Adding model 'Audio'
        db.create_table(u'multimedia_audio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('audio', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('embed', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('archivomp3', self.gf('django.db.models.fields.CharField')(max_length=500, blank=True)),
            ('fecha_ini', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('idcataudio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['multimedia.Cataudio'], null=True, blank=True)),
        ))
        db.send_create_signal(u'multimedia', ['Audio'])

        # Adding model 'Estadovideo'
        db.create_table(u'multimedia_estadovideo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('estadovideo', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
        ))
        db.send_create_signal(u'multimedia', ['Estadovideo'])

        # Adding model 'Video'
        db.create_table(u'multimedia_video', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('video', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('texto', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('claves', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('creacion', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('archivo', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('idusuario', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('preview', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('youtube', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('idtematica', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('cidoc', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('duracion', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('anho', self.gf('django.db.models.fields.CharField')(max_length=4, null=True, blank=True)),
            ('realizacion', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('idestadovideo', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('pais', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('googleid', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('titulo2', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
        ))
        db.send_create_signal(u'multimedia', ['Video'])


    def backwards(self, orm):
        # Deleting model 'Cataudio'
        db.delete_table(u'multimedia_cataudio')

        # Deleting model 'Audio'
        db.delete_table(u'multimedia_audio')

        # Deleting model 'Estadovideo'
        db.delete_table(u'multimedia_estadovideo')

        # Deleting model 'Video'
        db.delete_table(u'multimedia_video')


    models = {
        u'multimedia.audio': {
            'Meta': {'object_name': 'Audio'},
            'archivomp3': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'audio': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'embed': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fecha_ini': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idcataudio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['multimedia.Cataudio']", 'null': 'True', 'blank': 'True'})
        },
        u'multimedia.cataudio': {
            'Meta': {'object_name': 'Cataudio'},
            'cataudio': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ordencataudio': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'multimedia.estadovideo': {
            'Meta': {'object_name': 'Estadovideo'},
            'estadovideo': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'multimedia.video': {
            'Meta': {'object_name': 'Video'},
            'anho': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'archivo': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'cidoc': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'claves': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'creacion': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'duracion': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'googleid': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idestadovideo': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'idtematica': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'idusuario': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'pais': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'preview': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'realizacion': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'titulo2': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'video': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'youtube': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['multimedia']