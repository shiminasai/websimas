# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Historias'
        db.create_table(u'historia_historias', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('descripcion', self.gf('ckeditor.fields.RichTextField')()),
            ('foto', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'historia', ['Historias'])


    def backwards(self, orm):
        # Deleting model 'Historias'
        db.delete_table(u'historia_historias')


    models = {
        u'historia.historias': {
            'Meta': {'object_name': 'Historias'},
            'descripcion': ('ckeditor.fields.RichTextField', [], {}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'foto': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['historia']