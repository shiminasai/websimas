# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Historias.descripcion'
        db.alter_column(u'historia_historias', 'descripcion', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Historias.foto'
        db.alter_column(u'historia_historias', 'foto', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100, null=True))

    def backwards(self, orm):

        # Changing field 'Historias.descripcion'
        db.alter_column(u'historia_historias', 'descripcion', self.gf('ckeditor.fields.RichTextField')(default=2))

        # Changing field 'Historias.foto'
        db.alter_column(u'historia_historias', 'foto', self.gf(u'sorl.thumbnail.fields.ImageField')(default=2, max_length=100))

    models = {
        u'historia.historias': {
            'Meta': {'object_name': 'Historias'},
            'descripcion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'foto': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['historia']