# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Organizaciones'
        db.create_table(u'portafolio_organizaciones', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('iniciales', self.gf('django.db.models.fields.CharField')(max_length=150)),
        ))
        db.send_create_signal(u'portafolio', ['Organizaciones'])

        # Adding model 'Portafolio'
        db.create_table(u'portafolio_portafolio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('servicio', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('resumen', self.gf('django.db.models.fields.TextField')()),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
        ))
        db.send_create_signal(u'portafolio', ['Portafolio'])

        # Adding M2M table for field organizacion on 'Portafolio'
        m2m_table_name = db.shorten_name(u'portafolio_portafolio_organizacion')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('portafolio', models.ForeignKey(orm[u'portafolio.portafolio'], null=False)),
            ('organizaciones', models.ForeignKey(orm[u'portafolio.organizaciones'], null=False))
        ))
        db.create_unique(m2m_table_name, ['portafolio_id', 'organizaciones_id'])


    def backwards(self, orm):
        # Deleting model 'Organizaciones'
        db.delete_table(u'portafolio_organizaciones')

        # Deleting model 'Portafolio'
        db.delete_table(u'portafolio_portafolio')

        # Removing M2M table for field organizacion on 'Portafolio'
        db.delete_table(db.shorten_name(u'portafolio_portafolio_organizacion'))


    models = {
        u'portafolio.organizaciones': {
            'Meta': {'object_name': 'Organizaciones'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iniciales': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'portafolio.portafolio': {
            'Meta': {'object_name': 'Portafolio'},
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'organizacion': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['portafolio.Organizaciones']", 'symmetrical': 'False'}),
            'resumen': ('django.db.models.fields.TextField', [], {}),
            'servicio': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['portafolio']