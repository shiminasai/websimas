# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Contacto'
        db.create_table(u'contacto_contacto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('texto', self.gf('ckeditor.fields.RichTextField')()),
        ))
        db.send_create_signal(u'contacto', ['Contacto'])


    def backwards(self, orm):
        # Deleting model 'Contacto'
        db.delete_table(u'contacto_contacto')


    models = {
        u'contacto.contacto': {
            'Meta': {'object_name': 'Contacto'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texto': ('ckeditor.fields.RichTextField', [], {})
        }
    }

    complete_apps = ['contacto']