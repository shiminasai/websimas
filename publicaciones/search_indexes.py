import datetime
from haystack import indexes
from .models import Publicacion


class PublicacionIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    publicacion = indexes.CharField(model_attr='publicacion')
    resumen = indexes.CharField(model_attr='resumen')
    claves = indexes.CharField(model_attr='claves', null=True)

    content_auto = indexes.EdgeNgramField(model_attr='publicacion')

    def get_model(self):
        return Publicacion

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
