# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Tematica'
        db.create_table(u'publicaciones_tematica', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tematica', self.gf('django.db.models.fields.CharField')(max_length=300, blank=True)),
            ('uri', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('foto', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('texto', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('orden', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'publicaciones', ['Tematica'])

        # Adding model 'Tipopublicacion'
        db.create_table(u'publicaciones_tipopublicacion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipopublicacion', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
        ))
        db.send_create_signal(u'publicaciones', ['Tipopublicacion'])

        # Adding model 'Disponibilidad'
        db.create_table(u'publicaciones_disponibilidad', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('disponibilidad', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'publicaciones', ['Disponibilidad'])

        # Adding model 'Publicacion'
        db.create_table(u'publicaciones_publicacion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('autor', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('edicion', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('fecha', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('lugar', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('editorial', self.gf('django.db.models.fields.CharField')(max_length=2000, null=True, blank=True)),
            ('paginas', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('isbn', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('resumen', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('portada', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('precio', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
            ('idtipodocumento', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('iddisponibilidad', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['publicaciones.Disponibilidad'], null=True, blank=True)),
            ('nombre1', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('archivo1', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('nombre2', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('archivo2', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('nombre3', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('archivo3', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('nombre4', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('archivo4', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('nombre5', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('archivo5', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('nombre6', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('archivo6', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('tematicaanterior', self.gf('django.db.models.fields.CharField')(max_length=1500, null=True, blank=True)),
            ('actualizado', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('notas', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('publicar', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('idtipopublicacion', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('tipo_publicacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['publicaciones.Tipopublicacion'], null=True, blank=True)),
            ('publicacion', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('claves', self.gf('django.db.models.fields.CharField')(max_length=1500, null=True, blank=True)),
            ('enlace', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('uri', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('cidoc', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('archivo', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('titulo1', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('titulo2', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('titulo3', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('titulo4', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('titulo5', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('idtematica2', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('archivo7', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('nombre7', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=1, blank=True)),
            ('idpublicacion2', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('sinfecha', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('enportada', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'publicaciones', ['Publicacion'])

        # Adding M2M table for field idtematica on 'Publicacion'
        m2m_table_name = db.shorten_name(u'publicaciones_publicacion_idtematica')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('publicacion', models.ForeignKey(orm[u'publicaciones.publicacion'], null=False)),
            ('tematica', models.ForeignKey(orm[u'publicaciones.tematica'], null=False))
        ))
        db.create_unique(m2m_table_name, ['publicacion_id', 'tematica_id'])


    def backwards(self, orm):
        # Deleting model 'Tematica'
        db.delete_table(u'publicaciones_tematica')

        # Deleting model 'Tipopublicacion'
        db.delete_table(u'publicaciones_tipopublicacion')

        # Deleting model 'Disponibilidad'
        db.delete_table(u'publicaciones_disponibilidad')

        # Deleting model 'Publicacion'
        db.delete_table(u'publicaciones_publicacion')

        # Removing M2M table for field idtematica on 'Publicacion'
        db.delete_table(db.shorten_name(u'publicaciones_publicacion_idtematica'))


    models = {
        u'publicaciones.disponibilidad': {
            'Meta': {'object_name': 'Disponibilidad'},
            'disponibilidad': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'publicaciones.publicacion': {
            'Meta': {'object_name': 'Publicacion'},
            'actualizado': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'archivo': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'archivo1': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'archivo2': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'archivo3': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'archivo4': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'archivo5': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'archivo6': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'archivo7': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'cidoc': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'claves': ('django.db.models.fields.CharField', [], {'max_length': '1500', 'null': 'True', 'blank': 'True'}),
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'edicion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'editorial': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'enlace': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'enportada': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iddisponibilidad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['publicaciones.Disponibilidad']", 'null': 'True', 'blank': 'True'}),
            'idpublicacion2': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'idtematica': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['publicaciones.Tematica']", 'null': 'True', 'blank': 'True'}),
            'idtematica2': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'idtipodocumento': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'idtipopublicacion': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'isbn': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'lugar': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'nombre1': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'nombre2': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'nombre3': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'nombre4': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'nombre5': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'nombre6': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'nombre7': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'notas': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'paginas': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'portada': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'precio': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'publicacion': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'publicar': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'resumen': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'sinfecha': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'tematicaanterior': ('django.db.models.fields.CharField', [], {'max_length': '1500', 'null': 'True', 'blank': 'True'}),
            'tipo_publicacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['publicaciones.Tipopublicacion']", 'null': 'True', 'blank': 'True'}),
            'titulo1': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'titulo2': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'titulo3': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'titulo4': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'titulo5': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'publicaciones.tematica': {
            'Meta': {'object_name': 'Tematica'},
            'fecha': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'orden': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tematica': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        u'publicaciones.tipopublicacion': {
            'Meta': {'object_name': 'Tipopublicacion'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipopublicacion': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['publicaciones']