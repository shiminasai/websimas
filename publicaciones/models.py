# -*- coding: utf-8 -*-
from django.db import models
from ckeditor.fields import RichTextField
from sorl.thumbnail import ImageField
from django.template.defaultfilters import slugify
from websimas.utils import get_file_path
from django.core.urlresolvers import reverse

# Create your models here.


class Tematica(models.Model):
    tematica = models.CharField(max_length=300, blank=True)
    uri = models.CharField(max_length=150, blank=True)
    foto = models.CharField(max_length=255, blank=True)
    texto = models.TextField(blank=True)
    fecha = models.DateField(blank=True, null=True)
    orden = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name = u'Tematica'
        verbose_name_plural = u'Tematicas'

    def __unicode__(self):
        return self.tematica


class Tipopublicacion(models.Model):
    tipopublicacion = models.CharField(max_length=300, null=True, blank=True)

    class Meta:
        verbose_name = u'Tipo Publicación'
        verbose_name_plural = u'Tipos de publicaciones'

    def __unicode__(self):
        return self.tipopublicacion


class Disponibilidad(models.Model):
    disponibilidad = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        verbose_name = 'disponibilidad'
        verbose_name_plural = 'disponibilidades'

    def __unicode__(self):
        return self.disponibilidad


class Publicacion(models.Model):
    idtematica = models.ManyToManyField(Tematica, verbose_name="Tematicas",
                                        blank=True, null=True)
    autor = models.CharField(max_length=300, blank=True, null=True)
    edicion = models.CharField(max_length=200, blank=True, null=True)
    fecha = models.DateField(max_length=50, blank=True, null=True)
    lugar = models.CharField(max_length=200, blank=True, null=True)
    editorial = models.CharField(max_length=2000, blank=True, null=True)
    paginas = models.CharField(max_length=50, blank=True, null=True)
    isbn = models.CharField(max_length=50, blank=True, null=True)
    codigo = models.CharField(max_length=50, blank=True, null=True)
    resumen = RichTextField(blank=True, null=True)
    portada = ImageField(upload_to=get_file_path, blank=True, null=True)
    precio = models.DecimalField(max_digits=10, decimal_places=2,
                                blank=True, null=True)
    idtipodocumento = models.IntegerField(blank=True, null=True)
    iddisponibilidad = models.ForeignKey(Disponibilidad, verbose_name='Disponibilidad', 
                                blank=True, null=True)
    nombre1 = models.CharField(max_length=250, blank=True, null=True)
    archivo1 = models.FileField(upload_to=get_file_path, blank=True, null=True)
    nombre2 = models.CharField(max_length=250, blank=True, null=True)
    archivo2 = models.FileField(upload_to=get_file_path, blank=True, null=True)
    nombre3 = models.CharField(max_length=250, blank=True, null=True)
    archivo3 = models.FileField(upload_to=get_file_path, blank=True, null=True)
    nombre4 = models.CharField(max_length=250, blank=True, null=True)
    archivo4 = models.FileField(upload_to=get_file_path, blank=True, null=True)
    nombre5 = models.CharField(max_length=250, blank=True, null=True)
    archivo5 = models.FileField(upload_to=get_file_path, blank=True, null=True)
    nombre6 = models.CharField(max_length=250, blank=True, null=True)
    archivo6 = models.FileField(upload_to=get_file_path, blank=True, null=True)
    tematicaanterior = models.CharField(max_length=1500, blank=True, null=True)
    actualizado = models.NullBooleanField()
    notas = models.TextField(blank=True, null=True)
    publicar = models.NullBooleanField()
    idtipopublicacion = models.IntegerField(blank=True, null=True)
    tipo_publicacion = models.ForeignKey(Tipopublicacion, blank=True,
                                        null=True)
    publicacion = models.CharField(max_length=500, blank=True, null=True)
    claves = models.CharField(max_length=1500, blank=True, null=True)
    enlace = models.CharField(max_length=500, blank=True, null=True)
    uri = models.CharField(max_length=500, blank=True, null=True)
    cidoc = models.NullBooleanField()
    archivo = models.CharField(max_length=500, blank=True, null=True)
    titulo1 = models.CharField(max_length=300, blank=True, null=True)
    titulo2 = models.CharField(max_length=300, blank=True, null=True)
    titulo3 = models.CharField(max_length=300, blank=True, null=True)
    titulo4 = models.CharField(max_length=300, blank=True, null=True)
    titulo5 = models.CharField(max_length=300, blank=True, null=True)
    idtematica2 = models.IntegerField(blank=True, null=True)
    archivo7 = models.CharField(max_length=255, blank=True, null=True)
    nombre7 = models.CharField(max_length=255, blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True)
    idpublicacion2 = models.IntegerField(blank=True, null=True)
    sinfecha = models.NullBooleanField()
    enportada = models.NullBooleanField()

    fileDir = 'publicaciones/'

    def save(self, *args, **kwargs):
        self.uri = (slugify(self.publicacion))
        super(Publicacion, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('detalle_publicacion', kwargs={'uri': self.uri})

    class Meta:
        verbose_name = u'Publicación'
        verbose_name_plural = u'Publicaciones'
        ordering = ('-id',)

    def __unicode__(self):
        return self.publicacion