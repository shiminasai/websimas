# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView
from .models import Publicacion
from otras.models import MenuFoto

# Create your views here.


class ListBooksView(ListView):
    model = Publicacion
    queryset = Publicacion.objects.filter(cidoc=False).order_by('-fecha')
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(ListBooksView, self).get_context_data(**kwargs)
        context['foto_publi'] = MenuFoto.objects.get(menu=2)

        return context


def DetailBooksView(request, template='publicaciones/detalle_publicacion.html',
    id=None, uri=None):
    publi = get_object_or_404(Publicacion, id=id)
    foto_publi = MenuFoto.objects.get(menu=2)
    return render(request, template, locals())


class ListBookSimasView(ListView):
    model = Publicacion
    queryset = Publicacion.objects.filter(cidoc=False).order_by('-fecha')
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(ListBookSimasView, self).get_context_data(**kwargs)
        context['foto_publi'] = MenuFoto.objects.get(menu=2)

        return context


class ListBookCidocView(ListView):
    model = Publicacion
    queryset = Publicacion.objects.filter(cidoc=True).order_by('-id')
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(ListBookCidocView, self).get_context_data(**kwargs)
        context['foto_publi'] = MenuFoto.objects.get(menu=2)

        return context
