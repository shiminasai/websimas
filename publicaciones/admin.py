from django.contrib import admin
from .models import Tematica, Tipopublicacion, Publicacion


class PublicacionAdmin(admin.ModelAdmin):
    search_fields = ('publicacion', 'autor', 'editorial','claves','resumen')
    list_filter = ('idtematica', 'tipo_publicacion', 'cidoc')
    list_display = ('id', 'publicacion', 'codigo', 'autor', 'cidoc', 'editorial')
    list_display_links = ('id', 'publicacion')
    fieldsets = (
         (None, {
             'fields': (('publicacion', 'portada'), ('fecha', 'codigo', 'autor'), 
     					'resumen', ('claves', 'lugar'), 'notas', 'idtematica', ('cidoc','edicion', 
     					'editorial'), ('paginas', 'isbn', 'tipo_publicacion'), ('precio',
     					'iddisponibilidad', 'enportada'))
         			}),
         ('Archivos para adjuntar', {
             'fields': (('nombre1', 'archivo1'), ('nombre2','archivo2'),
                       ('nombre3', 'archivo3'),('nombre4', 'archivo4'),
    					('nombre5', 'archivo5'))
         }),
    )

# Register your models here.
admin.site.register(Tematica)
admin.site.register(Tipopublicacion)
admin.site.register(Publicacion, PublicacionAdmin)
