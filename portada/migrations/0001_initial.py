# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FotoPortada'
        db.create_table(u'portada_fotoportada', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('texto1', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('texto2', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('foto', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'portada', ['FotoPortada'])


    def backwards(self, orm):
        # Deleting model 'FotoPortada'
        db.delete_table(u'portada_fotoportada')


    models = {
        u'portada.fotoportada': {
            'Meta': {'object_name': 'FotoPortada'},
            'foto': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texto1': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'texto2': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['portada']