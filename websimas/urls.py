from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'', include('noticias.urls')),
    url(r'', include('publicaciones.urls')),
    url(r'', include('blogs.urls')),
    url(r'^busqueda/', include('googlesearch.urls')),
    #url(r'^admin/', include(admin_site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    #url(r'^busqueda/$', include('django_google_cse.urls')),
    url(r'^contacto_ajax/$', 'noticias.views.contacto_ajax', name='contacto_ajax'),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^search/', include('haystack.urls')),
    
    
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )
